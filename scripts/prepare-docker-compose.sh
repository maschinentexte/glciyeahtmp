#!/bin/sh
cat << EOF
version: "3"

networks:
  proxy:
    external: true
  internal:
    external: false

volumes:
  data:
    external:
      name: ${DOCKER_VOLUME:-$CI_PROJECT_NAME}

services:
  setup:
    image: alpine:latest
    command: ["mkdir", "-p", "/data/db"]
    volumes:
      - data:/data
  server:
    image: $DOCKER_IMAGE
    container_name: $CI_PROJECT_NAME-$CI_PROJECT_ID-$CI_COMMIT_REF_SLUG
    depends_on:
      - setup
    labels:
      - traefik.backend=$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG
      - traefik.frontend.rule=Host:$APP_DEPLOY_URL
      - traefik.docker.network=proxy
      - traefik.port=$APP_PORT
    environment:
      NODE_ENV: ${NODE_ENV:-production}
    volumes:
      - data:/data
    networks:
      - internal
      - proxy
EOF
