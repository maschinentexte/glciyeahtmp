FROM node:12-alpine

RUN apk -U --no-cache add mongodb
RUN npm install pm2 -g

WORKDIR /app

COPY package*.json ./

RUN npm ci --only=production

COPY . .

RUN mkdir -p data/db

VOLUME [ "/data" ]

EXPOSE 5000

CMD ["pm2-runtime", "process.json"]