'use strict';

const express = require('express');
const MongoClient = require('mongodb').MongoClient;
 const url = 'mongodb://localhost:27017';
 
// Database Name
const dbName = 'myproject';

// App
const app = express();

const withDB = (req, res, next) => {
  if (app.get('db')) {
    next()
  } else {
    MongoClient.connect(url, function(err, client) {
      if (err) {
        next(err)
      } else {
        app.set('db', client.db(dbName));
        next()
      }
    })
  }
}

app.get('/', (req, res) => {
  res.send(`Hello world! ${process.pid}`);
});

app.get('/insert', withDB, (req, res) => {
  const db = app.get('db')
  const collection = db.collection('documents');
  collection.insertMany([
    {created : new Date()}
  ], function(err, result) { 
    res.json(result)
  }) 
})

app.get('/list', withDB, (req, res) => {
  const db = app.get('db')
  const collection = db.collection('documents');
  collection.find({}).toArray(function(err, docs) {
    res.json(docs)
  })
})

app.get('/purge', withDB, (_, res) => {
    app.get('db').collection('documents').deleteMany({}, (err) => {
        if (err) throw err
        res.send('deleted')
    })
})

app.get('/err', () => {
    throw new Error('OH NO!')
})


module.exports = app